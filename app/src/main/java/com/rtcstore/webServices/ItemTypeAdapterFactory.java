package com.rtcstore.webServices;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.rtcstore.utils.Constant;


import java.io.IOException;

public class ItemTypeAdapterFactory implements TypeAdapterFactory {

    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
        final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
        final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);

        return new TypeAdapter<T>() {

            public void write(JsonWriter out, T value) throws IOException {
                delegate.write(out, value);
            }

            public T read(JsonReader in) throws IOException {

                JsonElement jsonElement = elementAdapter.read(in);

                if (jsonElement.isJsonObject()) {

                    final JsonObject jsonObject = jsonElement.getAsJsonObject();

                    int status;
                    String message;


                    if (jsonObject.has(Constant.KEY_STATUS) && jsonObject.has(Constant.KEY_MESSAGE)) {
                        status = jsonObject.get(Constant.KEY_STATUS).getAsInt();
                        message = jsonObject.get(Constant.KEY_MESSAGE).getAsString();
                        if (status == ApiConstant.ERRORCODEONE) {
                            jsonObject.remove(Constant.KEY_DATA);
                            throw new IOException(message);
                        } else if (status == ApiConstant.SUCCESS) {
                            if (jsonObject.get(Constant.KEY_DATA).isJsonObject()) {
                                jsonElement = jsonObject.get(Constant.KEY_DATA);
                                return delegate.fromJsonTree(jsonElement);
                            }
                        }
                    }
                }
                return delegate.fromJsonTree(jsonElement);
            }
        }.nullSafe();
    }

}
