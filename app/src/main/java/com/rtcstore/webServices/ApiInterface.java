package com.rtcstore.webServices;


import com.google.gson.JsonObject;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("doLogin")
    Call<JsonObject> doLogin(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("generateCode")
    Call<JsonObject> generateCode(@Field("store_id") int store_id,@Field("qty")String qty);

    @FormUrlEncoded
    @POST("getSummary")
    Call<JsonObject> getSummary(@Field("store_id") int store_id);
}