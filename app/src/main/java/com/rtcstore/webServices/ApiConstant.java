package com.rtcstore.webServices;

public class ApiConstant {

    public static final String NO_INTERNET = "No Internet Connection Available";
    public static final String TIMEOUT = "Connection Timeout";
    public static final String SERVER_NOT_RESPONDING = "Server is not responding.Please try again later";
    public static final String PARSE_ERROR = "Unable to parse response from server";
    public static final String SOMETHING_WRONG = "Something went wrong.Please try again later";

    public static final int SUCCESS = 0;
    public static final int ERRORCODEONE = -1;


    public static final String BASE_URL = "http://recycletocoin.com/app/user/";

}
