package com.rtcstore;

import android.app.Application;

import com.rtcstore.webServices.RestClient;


public class RtcStoreApplication extends Application {
    private RtcStoreApplication instance;
    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        new RestClient();
    }
}
