package com.rtcstore.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.JsonObject;
import com.rtcstore.R;
import com.rtcstore.databinding.ActivityGenerateQrCodeBinding;
import com.rtcstore.utils.Constant;
import com.rtcstore.utils.Logger;
import com.rtcstore.utils.Utils;
import com.rtcstore.webServices.RestClient;
import com.rtcstore.webServices.RetrofitCallback;

import retrofit2.Call;

public class GenerateQrCodeActivity extends BaseActivity implements View.OnClickListener {
    ActivityGenerateQrCodeBinding activityGenerateQrCodeBinding;
    @Override
    protected void initView() {
        activityGenerateQrCodeBinding = DataBindingUtil.setContentView(this,R.layout.activity_generate_qr_code);
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_generate_qr_code_tb);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        setSupportActionBar(toolbar);
        showBackButton();
        toolbar.setOnClickListener(this);
        activityGenerateQrCodeBinding.activityGenerateQrCodeTvCreateCode.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_generate_qr_code_tv_createCode:
                validateData();
                break;
            case R.id.activity_generate_qr_code_tb:
                onBackPressed();
                break;
        }
    }

    private void validateData() {
        final String quantity = activityGenerateQrCodeBinding.activityGenerateQrCodeEtQuantity.getText().toString();
        if(!TextUtils.isEmpty(quantity)){
            generateCode(quantity);
        }else{
            Logger.showSnackbar(GenerateQrCodeActivity.this,"Enter quantity");
        }
    }

    private void generateCode(String quantity) {
        int id = Integer.parseInt(Utils.getString(GenerateQrCodeActivity.this,Constant.UserId));
        Call<JsonObject> getQrCode = RestClient.getInstance().getApiInterface().generateCode(id,quantity);
        getQrCode.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get(Constant.KEY_STATUS).getAsInt()==0){
                    final String codeString=data.get("code_string").getAsString();
                    if(!TextUtils.isEmpty(codeString)){
                        Intent i = new Intent(GenerateQrCodeActivity.this,GeneratedQrActivity.class);
                        i.putExtra("codeString",codeString);
                        navigateToNextActivity(i,false);
                    }
                }
                else{
                    Logger.toast(GenerateQrCodeActivity.this,data.get("res_msg").getAsString());
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

}
