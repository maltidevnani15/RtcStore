package com.rtcstore.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.rtcstore.R;
import com.rtcstore.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity implements View.OnClickListener {
 private  ActivityMainBinding activityMainBinding;
    @Override
    protected void initView() {
       activityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        Toolbar toolbar =(Toolbar)findViewById(R.id.activity_main_toolbar);
        final ImageView logo=(ImageView)toolbar.findViewById(R.id.row_tb_logo);
        logo.setImageResource(R.drawable.ic_logo_green);
        activityMainBinding.activityMainIvClaimdTransaction.setOnClickListener(this);
        activityMainBinding.activityMainIvGenerateQr.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.activity_main_iv_claimdTransaction:
                i=new Intent(MainActivity.this,ClaimedDetailActivity.class);
                navigateToNextActivity(i,false);
                break;
            case R.id.activity_main_iv_generateQr:
                i=new Intent(MainActivity.this,GenerateQrCodeActivity.class);
                navigateToNextActivity(i,false);
                break;
        }
    }
}
