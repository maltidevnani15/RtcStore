package com.rtcstore.Activities;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.JsonObject;

import com.rtcstore.R;
import com.rtcstore.databinding.ActivityLoginBinding;
import com.rtcstore.utils.Constant;
import com.rtcstore.utils.Logger;
import com.rtcstore.utils.Utils;
import com.rtcstore.webServices.RestClient;
import com.rtcstore.webServices.RetrofitCallback;

import retrofit2.Call;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    ActivityLoginBinding activityLoginBinding;
    @Override
    protected void initView() {
        activityLoginBinding = DataBindingUtil.setContentView(this,R.layout.activity_login);
        activityLoginBinding.activityLoginTvLogin.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_login_tv_login:
                validateData();
                Utils.hideSoftKeyboard(this);
                break;
        }
    }

    private void validateData() {
        final String email = activityLoginBinding.activityLoginEtUserId.getText().toString();
        final String password = activityLoginBinding.activityLoginEtPwd.getText().toString();
        if(!TextUtils.isEmpty(email)){
            if(Utils.isEmailValid(email)){
                if(!TextUtils.isEmpty(password)){
                    doLogin(email,password);
                }else{
                    Logger.showSnackbar(this,getString(R.string.enter_password));
                }
            }else{
                Logger.showSnackbar(this,getString(R.string.email_not_valid));
            }
        }else{
            Logger.showSnackbar(this,getString(R.string.enter_email));
        }
    }

    private void doLogin(String email, String password) {
        Call<JsonObject>dologin = RestClient.getInstance().getApiInterface().doLogin(email,password);
        dologin.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get(Constant.KEY_STATUS).getAsInt()==0){
                    if(!TextUtils.isEmpty(data.get("user_id").getAsString())){
                        Utils.storeString(LoginActivity.this, Constant.UserId,data.get("user_id").getAsString());
                        Intent i = new Intent(LoginActivity.this,MainActivity.class);
                        navigateToNextActivity(i,true);
                    }
                }
                else{
                    Logger.toast(LoginActivity.this,data.get("res_msg").getAsString());
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });

    }
}
