package com.rtcstore.Activities;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.JsonObject;
import com.rtcstore.R;
import com.rtcstore.databinding.ActivityClaimedDetailBinding;
import com.rtcstore.utils.Constant;
import com.rtcstore.utils.Logger;
import com.rtcstore.utils.Utils;
import com.rtcstore.webServices.RestClient;
import com.rtcstore.webServices.RetrofitCallback;

import retrofit2.Call;

public class ClaimedDetailActivity extends BaseActivity implements View.OnClickListener {
    private ActivityClaimedDetailBinding activityClaimedDetailBinding;
    @Override
    protected void initView() {
        activityClaimedDetailBinding = DataBindingUtil.setContentView(this,R.layout.activity_claimed_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_claim_detail_tb);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        setSupportActionBar(toolbar);
        showBackButton();
        toolbar.setOnClickListener(this);
        getSummary();
    }



    private void getSummary() {
        int id = Integer.parseInt(Utils.getString(ClaimedDetailActivity.this,Constant.UserId));
        Log.e("store id",Utils.getString(ClaimedDetailActivity.this,Constant.UserId));
        Call<JsonObject>getSummary = RestClient.getInstance().getApiInterface().getSummary(id);
        getSummary.enqueue(new RetrofitCallback<JsonObject>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get(Constant.KEY_STATUS).getAsInt()==0){
                    if(!TextUtils.isEmpty(data.get("claimed_points").getAsString())){
                        activityClaimedDetailBinding.activityClaimedDetailTvBalance.setText(data.get("claimed_points").getAsString());
                    }if(!TextUtils.isEmpty(data.get("claimed_qty").getAsString())){
                        activityClaimedDetailBinding.activityClaimedDetailTvBottle.setText(data.get("claimed_qty").getAsString());

                    }
                } else{
                    Logger.toast(ClaimedDetailActivity.this,data.get("res_msg").getAsString());
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_claim_detail_tb:
                onBackPressed();
                break;
        }
    }
}
