package com.rtcstore.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;

import com.rtcstore.Activities.BaseActivity;
import com.rtcstore.R;
import com.rtcstore.databinding.ActivitySplashBinding;
import com.rtcstore.utils.Constant;
import com.rtcstore.utils.Logger;
import com.rtcstore.utils.Utils;

public class SplashActivity extends BaseActivity {
 ActivitySplashBinding activitySplashBinding;
    @Override
    protected void initView() {
        activitySplashBinding = DataBindingUtil.setContentView(this,R.layout.activity_splash);
        new SplashCountDown(2000,1000).start();
    }





    private class SplashCountDown extends CountDownTimer {
        /**
         * @param millisInFuture    The number of millis in the future from the call
         *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
         *                          is called.
         * @param countDownInterval The interval along the way to receive
         *                          {@link #onTick(long)} callbacks.
         */
        public SplashCountDown(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            if(Utils.isConnectedToInternet(SplashActivity.this)){
                if(!TextUtils.isEmpty(Utils.getString(SplashActivity.this, Constant.UserId))){
                    Log.e("user_id",Utils.getString(SplashActivity.this,Constant.UserId));
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    navigateToNextActivity(intent, true);
                }else{
                    Log.e("user_id",Utils.getString(SplashActivity.this,Constant.UserId));
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    navigateToNextActivity(intent, true);
                }

            }else{
                Logger.toast(SplashActivity.this,"No internet connection");
                finish();
            }

        }

    }

    @Override
    protected void onStart() {
        super.onStart();


    }
}
