package com.rtcstore.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.rtcstore.R;
import com.rtcstore.databinding.ActivityGeneratedQrBinding;

import net.glxn.qrgen.android.QRCode;


public class GeneratedQrActivity extends BaseActivity implements View.OnClickListener {
    private ActivityGeneratedQrBinding activityGenerateQrCodeBinding;
    private String codeString;
    @Override
    protected void initView() {
        activityGenerateQrCodeBinding = DataBindingUtil.setContentView(this,R.layout.activity_generated_qr);
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_generated_qr_tb);
        toolbar.setBackgroundColor(getResources().getColor(R.color.purple));
        setSupportActionBar(toolbar);
        showBackButton();
        if(getIntent()!=null){
            Intent i = getIntent();
            codeString = i.getStringExtra("codeString");
            generateQrCode(codeString);
        }
 toolbar.setOnClickListener(this);
    }


    private void generateQrCode(String codeString) {
        Bitmap myBitmap = QRCode.from(codeString).withSize(700,700).bitmap();
       activityGenerateQrCodeBinding.activityGeneratedQrIv.setImageBitmap(myBitmap);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this,MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        navigateToNextActivity(i,true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_generated_qr_tb:
                onBackPressed();
                break;
        }
    }
}
